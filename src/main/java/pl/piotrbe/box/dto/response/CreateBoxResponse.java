package pl.piotrbe.box.dto.response;

import pl.piotrbe.box.entity.Box;

public class CreateBoxResponse {

    private String boxCode;

    @Override
    public String toString() {
        return this.boxCode;
    }

    public CreateBoxResponse(Box box) {
        this.boxCode = box.getBoxCode();
    }
}
