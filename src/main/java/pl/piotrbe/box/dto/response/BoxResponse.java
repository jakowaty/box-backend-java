package pl.piotrbe.box.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
public class BoxResponse {
    
    private String code;
    private String text;
    private String deleteCode;
    private Date createdAt;
    private Date validTill;
    private ArrayList<FileEntry> files = new ArrayList<>();

    @Getter
    public static class FileEntry {
        final private String path;
        final private String name;
        final private String mimeType;
        final private String accessTokenCode;
        
        public FileEntry(String path, String name, String mimeType, String accessToken) {
            this.path = path;
            this.name = name;
            this.mimeType = mimeType;
            this.accessTokenCode = accessToken;
        }
    }
}
