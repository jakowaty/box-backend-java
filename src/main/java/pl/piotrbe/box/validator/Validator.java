package pl.piotrbe.box.validator;

import pl.piotrbe.box.exception.ValidationException;

public interface Validator <T> {
    public void validate(T entity) throws ValidationException;
}
