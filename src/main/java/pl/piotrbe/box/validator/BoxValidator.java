package pl.piotrbe.box.validator;

import org.springframework.stereotype.Service;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.exception.ValidationException;

@Service
public class BoxValidator implements Validator<Box> {
    @Override
    public void validate(Box entity) throws ValidationException {
        validateText(entity);
        validatePassword(entity);
    }

    private void validatePassword(Box entity) throws ValidationException {
        if (
            entity.isPasswordProtected() &&
            (
                entity.getPassword() == null ||
                entity.getPassword().isEmpty() ||
                entity.getPassword().length() < 8 ||
                entity.getPassword().length() > 150
            )
        ) {
            throw new ValidationException(
                "Password must be between 8 and 150 characters",
                entity.getClass()
            );
        }

        if (
            !entity.isPasswordProtected() &&
            (
                entity.getPassword() != null ||
                !entity.getPassword().isEmpty()
            )
        ) {
            throw new ValidationException(
                "Entity that is not protected must have empty password",
                entity.getClass()
            );
        }
    }

    private void validateText(Box entity) throws ValidationException {
        if (
                entity.getText() == null ||
                entity.getText().isEmpty() ||
                entity.getText().length() < 5 ||
                entity.getText().length() > 2000
        ) {
            throw new ValidationException(
                "Text can't be empty and must be between 5 and 2000 characters",
                entity.getClass()
            );
        }
    }
}
