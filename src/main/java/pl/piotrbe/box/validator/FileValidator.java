package pl.piotrbe.box.validator;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.piotrbe.box.exception.ValidationException;

@Service
public class FileValidator implements Validator<MultipartFile> {
    @Override
    public void validate(MultipartFile entity) throws ValidationException {
        String fileName = entity.getOriginalFilename();

        if (fileName == null || fileName.length() < 1 || fileName.length() > 150) {
            throw new ValidationException("File name must be between 1 and 150 characters", entity.getClass());
        }

    }
}
