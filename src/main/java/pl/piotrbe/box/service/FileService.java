package pl.piotrbe.box.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.piotrbe.box.configuration.FileStorageProperties;
import pl.piotrbe.box.entity.AccessToken;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.entity.File;
import pl.piotrbe.box.exception.ValidationException;
import pl.piotrbe.box.repository.AccessTokenRepository;
import pl.piotrbe.box.repository.FileRepository;
import pl.piotrbe.box.validator.FileValidator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {
    private final Path uploadedFilesPath;
    private final FileRepository fileRepository;
    private final StorageService storageService;
    private final AccessTokenRepository accessTokenRepository;
    private final FileValidator multipartFileValidator;

    @Autowired
    public FileService(
        FileStorageProperties fileStorageProperties,
        FileRepository repository,
        StorageService storageService,
        AccessTokenRepository accessTokenRepository,
        FileValidator fileValidator
    ) {
        this.fileRepository = repository;
        this.uploadedFilesPath = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        this.storageService = storageService;
        this.accessTokenRepository = accessTokenRepository;
        multipartFileValidator = fileValidator;
    }

    public List<File> uploadFiles(MultipartFile[] fromRequestFiles) throws IOException, ValidationException {
        List<File> uploadedFiles = new ArrayList<>();

        for (MultipartFile multipartFileValidated : fromRequestFiles) {
            multipartFileValidator.validate(multipartFileValidated);
        }

        for (MultipartFile multipartFile : fromRequestFiles) {
            String fileToken = storageService.generateFileName();
            String sanitizedFilename = sanitizeFileName(multipartFile.getOriginalFilename());
            String contentType = multipartFile.getContentType();
            contentType = contentType != null && !contentType.trim().isEmpty() ?
                contentType : "application/octet-stream";
            AccessToken fileAccesToken = new AccessToken(storageService.generateFileAccessTokenCode(), fileToken);

            multipartFile.transferTo(uploadedFilesPath.resolve(fileToken));

            File newUploadedFile = createFile(sanitizedFilename, contentType, fileToken, null);

            fileRepository.save(newUploadedFile);
            accessTokenRepository.save(fileAccesToken);
            uploadedFiles.add(newUploadedFile);
        }

        return uploadedFiles;
    }


    public byte[] readFileNode(String path) throws IOException {
        return Files.readAllBytes(uploadedFilesPath.resolve(path));
    }

    private String sanitizeFileName(String fileName) {
        return fileName.replaceAll("/", "").replaceAll("\\\\", "");
    }

    private File createFile(String name, String mimeType, String path, Box box) { return new File(name, path, mimeType, box); }

    public boolean deleteFile(File file) {
         if (deletePhysicalFile(uploadedFilesPath.resolve(file.getPath()).toString())) {
             fileRepository.delete(file);

             return true;
         }

         return false;
    }

    private boolean deletePhysicalFile(String path) {
        java.io.File phsycialFile = new java.io.File(path);

        return phsycialFile.delete();
    }
}
