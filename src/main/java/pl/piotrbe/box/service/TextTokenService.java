package pl.piotrbe.box.service;

import org.springframework.stereotype.Service;
import java.security.SecureRandom;

@Service
public class TextTokenService {
    public final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    public final String DIGIT = "0123456789";
    public final String SPECIAL = "_-";

    private String mode = null;

    public String getMode() {
        return mode;
    }

    public String selectMode(boolean upper, boolean lower, boolean digit, boolean special) {
        String oldMode = this.mode;
        StringBuilder newModeBuilder = new StringBuilder();

        if (upper) {
            newModeBuilder.append(UPPER);
        }

        if (lower) {
            newModeBuilder.append(LOWER);
        }

        if (digit) {
            newModeBuilder.append(DIGIT);
        }

        if (special) {
            newModeBuilder.append(SPECIAL);
        }

        this.mode = newModeBuilder.toString();

        return oldMode;
    }

    public String generate(int len) throws IllegalArgumentException {
        if (this.mode.length() <= 2) {
            throw new IllegalArgumentException("Mode is too short.");
        }

        if (len < 4 || len > 200) {
            throw new IllegalArgumentException("Lenght must be in range 4 to 200");
        }

        StringBuilder tokenBuilder = new StringBuilder(len);
        SecureRandom secureRandom = new SecureRandom();

        for (int i = 0; i < len; i++) {
            tokenBuilder.append(
                this.mode.charAt(secureRandom.nextInt(this.mode.length()))
            );
        }

        return tokenBuilder.toString();
    }
}
