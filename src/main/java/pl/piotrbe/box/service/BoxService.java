package pl.piotrbe.box.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.piotrbe.box.configuration.BoxProperties;
import pl.piotrbe.box.dto.response.BoxResponse;
import pl.piotrbe.box.entity.AccessToken;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.entity.File;
import pl.piotrbe.box.exception.BoxNotFoundException;
import pl.piotrbe.box.exception.BoxObsoleteException;
import pl.piotrbe.box.exception.ValidationException;
import pl.piotrbe.box.repository.BoxRepository;
import pl.piotrbe.box.repository.FileRepository;
import pl.piotrbe.box.validator.BoxValidator;

import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class BoxService {
    private final FileRepository fileRepository;
    private final StorageService storageService;

    @Getter
    private final BoxRepository boxRepository;

    private final PasswordsService passwordsService;
    private final FileService fileService;
    private final BoxProperties boxProperties;
    private final BoxValidator boxValidator;

    @Autowired
    public BoxService(FileRepository fileRepository, StorageService storageService, BoxRepository boxRepository, PasswordsService passwordsService, FileService fileService, BoxProperties boxProperties, BoxValidator boxValidator) {
        this.fileRepository = fileRepository;
        this.storageService = storageService;
        this.boxRepository = boxRepository;
        this.passwordsService = passwordsService;
        this.fileService = fileService;
        this.boxProperties = boxProperties;
        this.boxValidator = boxValidator;
    }

    public Box createBox(Boolean usePass, String boxPass, String boxText)
        throws IllegalArgumentException, ValidationException {
        Date now = new Date();
        Date valid = Date.from(now.toInstant().plus(
            boxProperties.getInDaysValidityAsInt(),
            ChronoUnit.DAYS)
        );
        Box box = new Box(boxText, passwordsService.encode(boxPass), usePass, new ArrayList<File>(), valid, now);
        String boxHash = storageService.generateBoxHash();

        box.setBoxCode(boxHash);
        boxValidator.validate(box);

        return box;
    }

    @Transactional(rollbackFor = {ValidationException.class})
    public Box saveBox(Box box) {
        return boxRepository.save(box);
    }

    public Box findBox(String boxCode) throws BoxNotFoundException, BoxObsoleteException {
        Optional<Box> boxPromise = this.boxRepository.findByBoxCode(boxCode);

        if (!boxPromise.isPresent()) {
            throw new BoxNotFoundException();
        }

        Box box = boxPromise.get();

        if (box.getValidTill().before(new Date())) {
            throw new BoxObsoleteException();
        }

        if (box.getDeletedAt() != null) {
            throw new BoxNotFoundException();
        }

        return box;
    }

    public Box addBoxFiles(Box box, List<File> boxFiles) {
        for (File file: boxFiles) {
            box.getFiles().add(file);
            file.setBox(box);
            fileRepository.save(file);
        }

        return boxRepository.save(box);
    }

    /**
     * Creates response for GET Box
     * @param box
     * @return
     */
    public BoxResponse createBoxResponse(Box box) {
        BoxResponse responseDto = new BoxResponse();
        AccessToken deleteToken = storageService.createAccessToken(box.getBoxCode());

        responseDto.setCode(box.getBoxCode());
        responseDto.setCreatedAt(box.getCreatedAt());
        responseDto.setValidTill(box.getValidTill());
        responseDto.setText(box.getText());
        responseDto.setDeleteCode(deleteToken.getTokenCode());

        box.getFiles().stream().forEach(file -> {
            AccessToken accessToken = storageService.createAccessToken(file);
            BoxResponse.FileEntry fileEntry = new BoxResponse.FileEntry(
                    file.getPath(),
                    file.getName(),
                    file.getMimeType(),
                    accessToken.getTokenCode()
            );
            responseDto.getFiles().add(fileEntry);
        });

        return responseDto;
    }

    public boolean softDeleteBox(Box box) {
        box.setDeletedAt(new Date());
        boxRepository.save(box);

        return true;
    }

    public void deleteBox(String boxCode) {
        Optional<Box> optionalBox = this.boxRepository.findByBoxCode(boxCode);

        if (optionalBox.isPresent()) {
            Box box = optionalBox.get();

            box.getFiles().forEach(fileService::deleteFile);
            boxRepository.delete(box);
        }
    }

    public void deleteBox(Box box) {
        box.getFiles().forEach(fileService::deleteFile);
        boxRepository.delete(box);
    }
}
