package pl.piotrbe.box.entityListener;

import org.springframework.beans.factory.annotation.Autowired;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.exception.ValidationException;
import pl.piotrbe.box.validator.BoxValidator;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class BoxListener {
    @Autowired
    private BoxValidator validator;

    @PrePersist
    @PreUpdate
    protected void doSaveValidation(Box box) throws ValidationException {
        validator.validate(box);
    }
}
