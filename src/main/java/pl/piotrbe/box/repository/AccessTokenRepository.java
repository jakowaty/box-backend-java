package pl.piotrbe.box.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import pl.piotrbe.box.entity.AccessToken;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public interface AccessTokenRepository extends CrudRepository<AccessToken, Long> {
    Optional<AccessToken> findByTokenCodeAndTokenValue(String tokenCode, String tokenValue);
    List<AccessToken> findAllByIsUsedTrue(Pageable pageable);
    List<AccessToken> findAllByIsUsedFalseAndCreatedAtIsLessThanEqual(Date date, Pageable pageable);
}
