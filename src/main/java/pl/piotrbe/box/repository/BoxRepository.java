package pl.piotrbe.box.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import pl.piotrbe.box.entity.Box;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public interface BoxRepository extends CrudRepository<Box, Long> {
    Optional<Box> findByBoxCode(String boxCode);
    List<Box> findAllByDeletedAtIsNotNull(Pageable pageable);
    List<Box> findByDeletedAtIsNullAndValidTillIsLessThanEqual(Date date, Pageable pageable);
}
