package pl.piotrbe.box.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import pl.piotrbe.box.entity.File;

@Service
public interface FileRepository extends CrudRepository<File, Long> {
}
