package pl.piotrbe.box.exception;

public class ValidationException extends Exception {
    private Class classProp;

    public ValidationException(String msg, Class classProp) {
        super(msg);
        this.classProp = classProp;
    }

    @Override
    public String getMessage() {
        return classProp.toString() + ":: " + super.getMessage();
    }
}
