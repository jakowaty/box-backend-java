package pl.piotrbe.box.entity;

import lombok.Getter;
import lombok.Setter;
import pl.piotrbe.box.entityListener.BoxListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "box")
@EntityListeners(BoxListener.class)
public class Box {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String boxCode;

    @Lob
    @Column(nullable = false, length = 2000)
    private String text;
    private String password;
    private boolean isPasswordProtected;

    @OneToMany(mappedBy = "box", fetch = FetchType.EAGER)
    private List<File> files = new ArrayList<>();

    private Date validTill;
    private Date createdAt;
    private Date deletedAt;

    public Box() {

    }

    public Box(
        String text,
        String password,
        boolean isPasswordProtected,
        List<File> files,
        Date validTill,
        Date createdAt
    ) {
        setText(text);
        setPassword(password);
        setPasswordProtected(isPasswordProtected);
        setFiles(files);
        setValidTill(validTill);
        setCreatedAt(createdAt);
        setDeletedAt(null);
    }
}
