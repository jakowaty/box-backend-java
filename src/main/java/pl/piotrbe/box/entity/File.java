package pl.piotrbe.box.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "file")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Column(unique = true)
    private String path;

    private String mimeType;

    @ManyToOne
    @JoinColumn(name = "box_id")
    private Box box;

    public File() {
    }

    public File (String name, String path, String mimeType, Box box) {
        setName(name);
        setPath(path);
        setBox(box);
        setMimeType(mimeType);
    }
}
