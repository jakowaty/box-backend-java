package pl.piotrbe.box.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@Entity
@Table(name = "file_access_token")
public class AccessToken {
    @Id
    @GeneratedValue
    private int id;

    @Column(length = 25, unique = true)
    private String tokenCode;

    // @todo add foreign key constraint
    private String tokenValue;

    private Date createdAt;
    private boolean isUsed;

    protected AccessToken() {}

    public AccessToken(String tokenCode, String tokenValue) {
        this.tokenCode = tokenCode;
        this.tokenValue = tokenValue;
        this.createdAt = new Date();
        this.isUsed = false;
    }

    public void use() {
        this.isUsed = true;
    }
}
