package pl.piotrbe.box.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.piotrbe.box.configuration.AccessTokenProperties;
import pl.piotrbe.box.entity.AccessToken;
import pl.piotrbe.box.repository.AccessTokenRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Component
public class DeleteObsoleteAccessTokens extends Task {
    private int inDaysValidity;
    private AccessTokenRepository tokenRepository;

    public DeleteObsoleteAccessTokens(
        @Autowired AccessTokenProperties tokenProperties,
        @Autowired AccessTokenRepository tokenRepository
    ) {
        super();
        this.tokenRepository = tokenRepository;
        inDaysValidity = tokenProperties.getInDaysValidityAsInt();
    }

    @Override
    @Scheduled(cron = "0 0 0 * * *")
    public void run() {
        this.logStartJob();

        LocalDate datePoint = LocalDate.now().minusDays(inDaysValidity);
        List<AccessToken> accessTokenList = tokenRepository.findAllByIsUsedFalseAndCreatedAtIsLessThanEqual(
            Date.valueOf(datePoint),
            PageRequest.of(0,100, Sort.Direction.ASC, "id")
        );

        this.logInformation("Starting point to search: " + datePoint.toString());
        this.logInformation("Items to process: " + accessTokenList.size());

        tokenRepository.deleteAll(accessTokenList);

        this.logEndJob();
    }

    @Override
    public String getDescription() {
        return "Delete obsolete AccessTokens";
    }
}
