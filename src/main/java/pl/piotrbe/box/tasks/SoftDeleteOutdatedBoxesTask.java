package pl.piotrbe.box.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.service.BoxService;

import java.util.Date;
import java.util.List;

@Component
public class SoftDeleteOutdatedBoxesTask extends Task {

    private final BoxService boxService;

    public SoftDeleteOutdatedBoxesTask(@Autowired BoxService boxService) {
        super();
        this.boxService = boxService;
    }

    @Override
    @Scheduled(cron = "0 0 1 * * *")
    public void run() {
        this.logStartJob();

        List<Box> boxes = boxService.getBoxRepository().findByDeletedAtIsNullAndValidTillIsLessThanEqual(
            new Date(),
            PageRequest.of(0,100, Sort.Direction.ASC, "id")
        );

        this.logInformation("To process: " + boxes.size() + " items.");

        for (Box b : boxes) {
            boxService.softDeleteBox(b);
        }

        this.logEndJob();
    }

    @Override
    public String getDescription() {
        return "Mark obsolete boxes as Soft-Deleted.";
    }
}
