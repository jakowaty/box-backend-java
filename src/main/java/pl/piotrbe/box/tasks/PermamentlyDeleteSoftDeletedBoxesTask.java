package pl.piotrbe.box.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.piotrbe.box.configuration.BoxProperties;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.service.BoxService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class PermamentlyDeleteSoftDeletedBoxesTask extends Task {
    private final BoxService boxService;
    private final BoxProperties boxProperties;

    public PermamentlyDeleteSoftDeletedBoxesTask(@Autowired BoxService boxService, @Autowired BoxProperties boxProperties) {
        super();
        this.boxService = boxService;
        this.boxProperties = boxProperties;
    }

    @Override
    @Scheduled(cron = "0 0 2 * * *")
    public void run() {
        this.logStartJob();

        List<Box> boxes = boxService.getBoxRepository().findAllByDeletedAtIsNotNull(
            PageRequest.of(0,100, Sort.Direction.ASC, "id")
        );

        this.logInformation("To process: " + boxes.size() + " items.");

        for (Box b : boxes) {
            Date boxSoftdeleteDate = b.getDeletedAt();
            Calendar calendar = Calendar.getInstance();

            calendar.setTime(boxSoftdeleteDate);
            calendar.add(Calendar.DATE, boxProperties.getInDaysValidityAsInt());

            if (calendar.getTime().before(new Date())) {
                boxService.deleteBox(b);
            }
        }

        this.logStartJob();
    }

    @Override
    public String getDescription() { return "Delete previously marked boxes and their files."; }
}
