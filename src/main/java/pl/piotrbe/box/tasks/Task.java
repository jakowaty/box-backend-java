package pl.piotrbe.box.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Task {
    protected final Logger logger;

    public Task() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    public void logStartJob() {
        logger.info("STARTING: " + this.getClass().getName() + System.lineSeparator() + this.getDescription());
    }

    public void logEndJob() {
        logger.info("ENDING" + this.getClass().getName() + System.lineSeparator() + this.getDescription());
    }

    public void logInformation(String message) {
        logger.info("JOB " + this.getClass().getName() + System.lineSeparator() + message);
    }

    abstract public void run();
    abstract public String getDescription();
}
