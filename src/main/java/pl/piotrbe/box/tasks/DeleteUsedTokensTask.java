package pl.piotrbe.box.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.piotrbe.box.entity.AccessToken;
import pl.piotrbe.box.repository.AccessTokenRepository;

import java.util.List;

@Component
public class DeleteUsedTokensTask extends Task {

    private final AccessTokenRepository accessTokenRepository;

    public DeleteUsedTokensTask(@Autowired AccessTokenRepository accessTokenRepository) {
        super();
        this.accessTokenRepository = accessTokenRepository;
    }

    @Override
    @Scheduled(cron = "0 0 3 * * *")
    public void run() {
        this.logStartJob();

        List<AccessToken> tokens = accessTokenRepository
            .findAllByIsUsedTrue(PageRequest.of(0, 100, Sort.Direction.ASC, "id"));

        this.logInformation("To process: " + tokens.size() + " items.");

        for (AccessToken ac : tokens) {
            accessTokenRepository.delete(ac);
        }

        this.logEndJob();
    }

    @Override
    public String getDescription() {
        return "Deletes used AccessTokens.";
    }
}
