package pl.piotrbe.box.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "access-token")
public class AccessTokenProperties {

    @Getter
    @Setter
    private String inDaysValidity;

    public int getInDaysValidityAsInt() {
        return Integer.parseInt(getInDaysValidity());
    }
}
