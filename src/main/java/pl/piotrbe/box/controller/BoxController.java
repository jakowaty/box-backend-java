package pl.piotrbe.box.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.piotrbe.box.dto.response.BoxResponse;
import pl.piotrbe.box.dto.response.CreateBoxResponse;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.entity.File;
import pl.piotrbe.box.exception.BoxNotFoundException;
import pl.piotrbe.box.exception.BoxObsoleteException;
import pl.piotrbe.box.exception.InvalidBoxCodeOrPasswordException;
import pl.piotrbe.box.exception.ValidationException;
import pl.piotrbe.box.service.BoxService;
import pl.piotrbe.box.service.FileService;
import pl.piotrbe.box.service.PasswordsService;
import pl.piotrbe.box.service.StorageService;

import java.util.Arrays;
import java.util.List;


@RestController
public class BoxController {

    @Autowired
    private BoxService boxService;

    @Autowired
    private FileService fileService;

    @Autowired
    private PasswordsService passwordsService;

    @Autowired
    private StorageService storageService;

    @PostMapping("/box")
    public ResponseEntity<String> createBox(
        @RequestParam("box_files") MultipartFile[] files,
        @RequestParam("box_text") String text,
        @RequestParam("use_pass") Boolean isPassword,
        @RequestParam("box_pass") String password
    ) {
        HttpHeaders headers = new HttpHeaders();
        //headers.set("Access-Control-Allow-Origin", "*");

        try {
            Box newBox = boxService.createBox(isPassword, password, text);
            List<File> uploadedFiles = fileService.uploadFiles(files);

            boxService.saveBox(newBox);
            boxService.addBoxFiles(newBox, uploadedFiles);

            CreateBoxResponse boxResponse = new CreateBoxResponse(newBox);

            return ResponseEntity.ok().headers(headers).body(boxResponse.toString());
        } catch (ValidationException validationException) {
            return ResponseEntity
                .status(HttpStatus.PRECONDITION_FAILED.value())
                .headers(headers)
                .body(validationException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            exception.printStackTrace();
            return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .headers(headers).body(exception.getMessage());
        }
    }

    @GetMapping("/box/{boxCode}")
    public ResponseEntity<BoxResponse> getBox(@PathVariable("boxCode") String boxCode, @RequestParam("boxPassword") String boxPassword) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus status = null;
        Box searchedBox = null;

        // headers.set("Access-Control-Allow-Origin", "*");

        try {
            searchedBox = boxService.findBox(boxCode);

            if (!passwordsService.compare(searchedBox.getPassword(), boxPassword)) {
                throw new InvalidBoxCodeOrPasswordException();
            }

            status = HttpStatus.OK;

            return ResponseEntity.status(status).headers(headers).body(boxService.createBoxResponse(searchedBox));
        } catch (BoxObsoleteException boxObsoleteException) {
            status = HttpStatus.GONE;
        } catch (BoxNotFoundException boxNotFoundException) {
            status = HttpStatus.NOT_FOUND;
        } catch (InvalidBoxCodeOrPasswordException invalidBoxCodeOrPasswordException) {
            status = HttpStatus.FORBIDDEN;
        } catch (Exception exception) {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return ResponseEntity.status(status).headers(headers).body(null);
    }

    @PostMapping("/box/delete/{boxCode}/{deleteCode}")
    public ResponseEntity<String> deleteBox(@PathVariable("boxCode") String boxcode,
          @PathVariable("deleteCode") String deleteCode) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = null;
        //headers.set("Access-Control-Allow-Origin", "*");

        try {
            if (storageService.checkAndMarkAccessTokenAsUsed(deleteCode, boxcode)) {
                boxService.softDeleteBox(boxService.findBox(boxcode));
                httpStatus = HttpStatus.OK;
            } else {
                httpStatus = HttpStatus.FORBIDDEN;
            }
        } catch (BoxObsoleteException boxObsoleteException) {
            httpStatus = HttpStatus.CONFLICT;
        } catch (BoxNotFoundException boxNotFoundException) {
            httpStatus = HttpStatus.NOT_FOUND;
        }

        return ResponseEntity
            .status(httpStatus)
            .headers(headers)
            .body(
                new StringBuilder().append(boxcode).append(" ").append(deleteCode).toString()
            );
    }
}
