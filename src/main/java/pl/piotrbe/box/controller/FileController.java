package pl.piotrbe.box.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.piotrbe.box.entity.AccessToken;
import pl.piotrbe.box.entity.Box;
import pl.piotrbe.box.entity.File;
import pl.piotrbe.box.repository.AccessTokenRepository;
import pl.piotrbe.box.repository.BoxRepository;
import pl.piotrbe.box.service.FileService;
import pl.piotrbe.box.service.StorageService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class FileController {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private BoxRepository boxRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    FileService fileService;

    @GetMapping("/file/{internalPath}/{accessToken}/box/{boxCode}")
    public ResponseEntity<byte[]> getFile(
        @PathVariable("internalPath") String internalPath,
        @PathVariable("accessToken") String accessToken,
        @PathVariable("boxCode") String boxCode
    ) {
        // @todo REFACTOR

        Optional<Box> boxOptional = boxRepository.findByBoxCode(boxCode);
        List<File> tiedFiles = null;
        HttpStatus responseStatus = null;

        // checks if box exists 404
        if (!boxOptional.isPresent()) {
            responseStatus = HttpStatus.NOT_FOUND;
        } else {
            // search for files matching {internalPath}
            tiedFiles = boxOptional.get()
                    .getFiles()
                    .stream()
                    .filter(f -> f.getPath().equals(internalPath))
                    .collect(Collectors.toList());
        }

        // if we have no status yet what implies error
        // if files are empty we throw 404
        if ((tiedFiles == null || tiedFiles.isEmpty()) && (responseStatus == null)) {
            responseStatus = HttpStatus.NOT_FOUND;
        }

        // if we have no status yet what implies error
        // files path should be unique 409
        if ((tiedFiles.size() != 1) && (responseStatus == null)) {
            responseStatus = HttpStatus.CONFLICT;
        }

        Optional<AccessToken> requestedFileAccessTokenOptional =
            accessTokenRepository.findByTokenCodeAndTokenValue(accessToken, tiedFiles.get(0).getPath());

        // if we have no status yet what implies error
        // invalid file access token 401
        if (!requestedFileAccessTokenOptional.isPresent() && responseStatus == null) {
            responseStatus = HttpStatus.FORBIDDEN;
        }

        // if we have no status yet what implies error
        // if token was used - token must be alwas generated
        if (requestedFileAccessTokenOptional.get().isUsed() && responseStatus == null) {
            responseStatus = HttpStatus.FORBIDDEN;
        }

        try {
            // if we have no status yet what implies error
            // everything ok - serve file
            
            if (responseStatus == null && storageService.checkAndMarkAccessTokenAsUsed(
                    requestedFileAccessTokenOptional.get().getTokenCode(),
                    requestedFileAccessTokenOptional.get().getTokenValue()
            )) {
                File file = tiedFiles.get(0);

                return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.parseMediaType(file.getMimeType()))
                    .header(
                        HttpHeaders.CONTENT_DISPOSITION,
                        new StringBuilder().append("attachment; filename=\"").append(file.getName()).append("\"").toString()
                    )
                    .body(fileService.readFileNode(file.getPath())
                );
            }
        } catch (IOException ioException) {
            responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return ResponseEntity.status(responseStatus).body(null);
    }
}
