package pl.piotrbe.box;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import pl.piotrbe.box.configuration.AccessTokenProperties;
import pl.piotrbe.box.configuration.BoxProperties;
import pl.piotrbe.box.configuration.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class,
    BoxProperties.class,
    AccessTokenProperties.class
})
@EnableScheduling
@EntityScan(basePackages = {"pl.piotrbe.box"})
public class BoxApplication {
    public static void main(String[] args) {
        SpringApplication.run(BoxApplication.class, args);
    }
}
